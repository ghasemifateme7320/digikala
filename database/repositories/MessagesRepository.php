<?php


namespace Database\repositories;

use Illuminate\Support\Facades\DB;



 class MessagesRepository
{

    public static function saveSmsToDb($body, $number, $provider, $status)
    {
        DB::insert('insert into messages (number, body, status, provider) values (?, ?, ?, ?)',
            [$number, $body, $status, $provider]);
//   return MessagesRepository::createMessagePendingMessage($number, $body);
    }

    public static function countAllMessages()
    {
        $count = DB::table('messages')->count();
        return $count;
    }

    public static function countAllMessagesByProvider($provider)
    {
        $count = DB::table('messages')
            ->where('provider', '=', $provider)
            ->count();
        return $count;
    }

    public static function countAllSuccessMessagesByProvider($provider)
    {
        $count = DB::table('messages')
            ->where('provider', '=', $provider)
            ->where('status', '=', 'success')
            ->count();
        return $count;
    }


    public static function mostUsedNumber()
    {
        $message = DB::table('messages')
            ->select('number')
            ->groupBy('number')
            ->orderByRaw('COUNT(*) DESC')
            ->limit(1)
            ->get();

        foreach ($message as $target_result) {
            return $target_result->number;
        }
    }

}
