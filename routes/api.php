<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Database\repositories\MessagesRepository as MessagesRepository;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


function saveSmsToDb($body, $number, $provider, $status)
{
    DB::insert('insert into messages (number, body, status, provider) values (?, ?, ?, ?)',
        [$number, $body, $status, $provider]);
//   return MessagesRepository::createMessagePendingMessage($number, $body);
}

function countAllMessages()
{
    $count = DB::table('messages')->count();
    return $count;
}

function countAllMessagesByProvider($provider)
{
    $count = DB::table('messages')
        ->where('provider', '=', $provider)
        ->count();
    return $count;
}

function countAllSuccessMessagesByProvider($provider)
{
    $count = DB::table('messages')
        ->where('provider', '=', $provider)
        ->where('status', '=', 'success')
        ->count();
    return $count;
}

function mostUsedNumber()
{
    $message = DB::table('messages')
        ->select('number')
        ->groupBy('number')
        ->orderByRaw('COUNT(*) DESC')
        ->limit(1)
        ->get();

    foreach ($message as $target_result) {
        return $target_result->number;
    }
}

Route::get('/report', function (Request $request) {
    $count = countAllMessages();

    $provider1 = 'Provider1';
    $provider2 = 'Provider2';
    $provider1Count = countAllMessagesByProvider($provider1);
    $provider2Count = countAllMessagesByProvider($provider2);
    $provider1SuccessCount = countAllSuccessMessagesByProvider($provider1);
    $provider2SuccessCount = countAllSuccessMessagesByProvider($provider2);
    $mostUsedNumber = mostUsedNumber();
    return [
        'allMessages' => $count,
        $provider1 => $provider1Count,
        $provider2 => $provider2Count,
        'mostUsedNumber' => $mostUsedNumber,
        'successRatio' => [
            $provider1 => $provider1SuccessCount / $provider1Count,
            $provider2 => $provider2SuccessCount / $provider2Count
        ]
    ];
});

Route::get('/sms/send', function (Request $request) {

    $number = $request->input('number');
    $body = $request->input('body');
//    saveSmsToDb($body, $number);

    $client = new Client();
    // Calling a test API
    $url1 = "https://digikala-sms-provider.herokuapp.com/firstProvider?number={$number}&body={$body}";
    $url2 = "https://digikala-sms-provider.herokuapp.com/secondProvider?number={$number}&body={$body}";
    try {
        $response = $client->request('GET', $url1);
        saveSmsToDb($body, $number, 'Provider1', 'success');
        return [
            'message' => "Provider1 send message successfully"
        ];
    } catch (Exception $e) {
        saveSmsToDb($body, $number, 'Provider1', 'fail');

        try {
            $response = $client->request('GET', $url2);

            saveSmsToDb($body, $number, 'Provider2', 'success');
            return [
                'message' => "Provider2 send message successfully"
            ];

        } catch (Exception $e) {
            saveSmsToDb($body, $number, 'Provider2', 'fail');
            return ["message"=>"None of providers answered. Please try again."];
        }


    }


});

