<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<div>
    <?php
    $report = [
        "allMessages" => 82,
        "Provider1" => 30,
        "Provider2" => 52,
        "mostUsedNumber" => "09360475919",
        "successRatio" => [
            "Provider1" => 0.4,
            "Provider2" => 0.15384615384615385
        ]
    ]
    //    echo $report;
    ?>

    <H3>SMS Reports</H3>
    <table id="myTable" style="width:100%">

    </table>

</div>
<script>
    function addRow(title, value) {
        const table = document.getElementById("myTable");
        const row = table.insertRow(-1);
        const cell1 = row.insertCell(0);
        const cell2 = row.insertCell(1);
        cell1.innerHTML = title;
        cell2.innerHTML = value;
    }

    function updateReport(){
        fetch('http://165.227.135.80:8000/report')
            .then(response => response.json())
            .then(data => {
                console.log(data)
                const table = document.getElementById("myTable");
                while(table.hasChildNodes())
                {
                    table.removeChild(table.firstChild);
                }
                addRow('All messages count',  data.allMessages)
                addRow('First provider requests count',  data.Provider1)
                addRow('First provider  success ratio',  data.successRatio.Provider1)
                addRow('Second provider requests count',  data.Provider2)
                addRow('Second provider  success ratio',  data.successRatio.Provider2)
                addRow('Most used mobile as destination',  data.mostUsedNumber)
                document.getElementById("allMessages").value = data.allMessages;
                document.getElementById("Provider1").value = data.Provider1;
                document.getElementById("Provider2").value = data.Provider2;
                document.getElementById("mostUsedNumber").value = data.mostUsedNumber;
            });
    }

    updateReport()
    const updateInterval = 1*60*1000 // 1 minute
    setInterval(updateReport, updateInterval)

</script>

<style>
    table, th, td {
        border: 1px solid black;
    }
    table{
        width: 50%  !important;

    }
    td {
        background-color: powderblue;
        font-weight:bold;
        padding: 5px;
        text-align:left;
    }

    th {
        background-color: powderblue;
        margin: 5px;
        color: blue
    }
</style>


</html>
