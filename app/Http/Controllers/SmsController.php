<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

public class SmsController extends Controller
{
    public static function sendSms($request)
    {
        $number = $request->input('number');
        $body = $request->input('body');
        saveSmsToDb($body, $number);

        $client = new Client();

        // Calling a test API
        $url = "https://api.github.com/users/kingsconsult/repos?number={$number}&body={$body}";
        $response = $client->request('GET', $url, [
            'verify' => false,
        ]);

        $responseBody = json_decode($response->getBody());
//        return [
//            'url' => $url,
//            'response' => $responseBody
//        ];

        $messages = DB::select('select * from messages where active = ?', [1]);

        return $messages;
    }

}
